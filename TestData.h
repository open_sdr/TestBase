/*
 * TestData.h
 *
 *  Created on: 26 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_TESTDATA_H_
#define SYS_TESTDATA_H_

#include <sdr_test_system_common.h>
#include "TestMethod.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  void * Master;
  TestMethod_t mtd;

  const char * name;
} TestDataCfg_t;

typedef struct
{
  void * Master;
  TestMethod_t mtd;

  const char * name;

  void * InternalData;
} TestData_t;

SYS_INLINE void init_TestData(TestData_t * This, TestDataCfg_t * Cfg)
{
  This->Master = Cfg->Master;
  This->mtd = Cfg->mtd;
  SYS_ASSERT(This->mtd.exec);

  This->name = Cfg->name;

  This->InternalData = 0;
}

SYS_INLINE void TestData_setInternalData(TestData_t * This, void * Data){This->InternalData = Data;}

SYS_INLINE const char * testdata_name(TestData_t * This){return This->name;}

SYS_INLINE void testdata_control_format(TestData_t * This, char * buf, Size_t size)
{
  if(This->mtd.get_control_format)
    This->mtd.get_control_format(This->Master, buf, size);
  else
    buf[0] = '\0';
}

SYS_INLINE void testdata_status_format(TestData_t * This, char * buf, Size_t size)
{
  if(This->mtd.get_status_format)
    This->mtd.get_status_format(This->Master, buf, size);
  else
    buf[0] = '\0';
}

SYS_INLINE void testdata_plots_format(TestData_t * This, char * buf, Size_t size)
{
  if(This->mtd.get_plots_format)
    This->mtd.get_plots_format(This->Master, buf, size);
  else
    buf[0] = '\0';
}

SYS_INLINE void testdata_refresh_state(TestData_t * This){if(This->mtd.refresh_state)This->mtd.refresh_state(This->Master);}
SYS_INLINE void testdata_sys_init(TestData_t * This){if(This->mtd.sys_init)This->mtd.sys_init(This->Master);}
SYS_INLINE void testdata_sys_wakeup(TestData_t * This){if(This->mtd.sys_wakeup)This->mtd.sys_wakeup(This->Master);}
SYS_INLINE void testdata_sys_destroy(TestData_t * This){if(This->mtd.sys_destroy)This->mtd.sys_destroy(This->Master);}
SYS_INLINE void testdata_prestart(TestData_t * This){if(This->mtd.prestart)This->mtd.prestart(This->Master);}
SYS_INLINE void testdata_exec(TestData_t * This){This->mtd.exec(This->Master);}
SYS_INLINE void testdata_reset(TestData_t * This){This->mtd.reset(This->Master);}
SYS_INLINE void testdata_prestop(TestData_t * This){if(This->mtd.prestop)This->mtd.prestop(This->Master);}

SYS_INLINE const char * testdata_ValueIdString(TestData_t * This, UInt8_t id)
{
  if(This->mtd.value_id_str)
    return This->mtd.value_id_str(This->Master, id);
  return "";
}
SYS_INLINE void testdata_set_value(TestData_t * This, UInt8_t id, const char * val, Size_t size){This->mtd.set_value(This->Master,id,val,size);}

#ifdef __cplusplus
}
#endif

#endif /* SYS_TESTDATA_H_ */
